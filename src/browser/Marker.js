class Marker {

    static install(AMap) {
        Marker.AMarker = AMap.Marker;
    }

    constructor(marker, title, map, index) {
        this.marker = marker;
        this.title = title;
        this.map = map;
        this.index = index;
    }

    remove() {
        this.map.unmark(this.index);
    }

}

Marker.AMarker = null;
export default Marker;