import FAMap from "./src/browser/FAMap";
import Map from './src/browser/Map';
import Marker from "./src/browser/Marker";

export {Map, Marker};
export default FAMap;